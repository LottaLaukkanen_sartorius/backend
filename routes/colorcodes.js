const router = require('express').Router();
let Colorcode = require('../models/color.model');

router.route('/').get((req, res) => {
    Colorcode.find()
        .then(logs => res.json(logs))
        .catch(err => res.status(400).json(`Error: ` + err));
});

router.route('/add').post((req, res) => {
    const name = req.body.name;
    const hexcode = req.body.hexcode;
    const DL = req.body.DL;
    const DH = req.body.DH;

    const newColorcode = new Colorcode({
        name,
        hexcode,
        DL,
        DH
    });

    newColorcode.save()
        .then(() => res.json('Color saved!'))
        .catch(err => res.status(400).json(`Error: ` + err));
});

module.exports = router;
const router = require('express').Router();
let displayCommand = require('../models/displaycommand.model');

router.route('/').get((req, res) => {
    displayCommand.find()
        .then(logs => res.json(logs))
        .catch(err => res.status(400).json(`Error: ` + err));
});

router.route('/add').post((req, res) => {
    const commandShort = req.body.commandShort;
    const commandName = req.body.commandName;
    const commandDescription = req.body.commandDescription;
    const response = req.body.response;

    const newDisplayCommand = new displayCommand({
        commandShort,
        commandName,
        commandDescription,
        response
    });

    newDisplayCommand.save()
        .then(() => res.json('Displaycommand created!'))
        .catch(err => res.status(400).json(`Error: ` + err));
});

module.exports = router;
const router = require('express').Router();
let LogItem = require('../models/logItem.model');

router.route('/').get((req, res) => {
    LogItem.find()
        .then(logs => res.json(logs))
        .catch(err => res.status(400).json(`Error: ` + err));
});

router.route('/:id').get((req, res) => {
    LogItem.find( {'userID': req.params.id} )
        .then(logs => res.json(logs))
        .catch(err => res.status(400).json(`Error: ` + err));
});

router.route('/device/:id').get((req, res) => {
    LogItem.find( {'deviceID': req.params.id} )
        .then(logs => res.json(logs))
        .catch(err => res.status(400).json(`Error: ` + err));
})

router.route('/add').post((req, res) => {
    const timestamp = Date.parse(req.body.timestamp);
    const logID = req.body.logID;
    const userID = req.body.userID;
    const device = req.body.device;
    const deviceID = req.body.deviceID;
    const success = req.body.success;
    const commandShort = req.body.commandShort;
    const commandName = req.body.commandName;
    const sent = req.body.sent;
    const response = req.body.response;
    const fullData = req.body.fullData;

    const newLog = new LogItem({
        timestamp,
        logID,
        userID,
        device,
        deviceID,
        success,
        commandShort,
        commandName,
        sent,
        response,
        fullData
    });

    newLog.save()
        .then(() => res.json('Event logged!'))
        .catch(err => res.status(400).json(`Error: ` + err));
});

module.exports = router;
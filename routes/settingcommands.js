const router = require('express').Router();
let settingCommand = require('../models/settingcommand.model');

router.route('/').get((req, res) => {
    settingCommand.find()
        .then(logs => res.json(logs))
        .catch(err => res.status(400).json(`Error: ` + err));
});

router.route('/add').post((req, res) => {
    const commandShort = req.body.commandShort;
    const commandName = req.body.commandName;
    const commandDescription = req.body.commandDescription;
    const parameter = req.body.parameter;

    const newSettingCommand = new settingCommand({
        commandShort,
        commandName,
        commandDescription,
        parameter
    });

    newSettingCommand.save()
        .then(() => res.json('Settingcommand created!'))
        .catch(err => res.status(400).json(`Error: ` + err));
});

module.exports = router;
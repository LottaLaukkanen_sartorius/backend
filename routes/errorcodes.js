const router = require('express').Router();
let Errorcode = require('../models/error.model');

router.route('/').get((req, res) => {
    Errorcode.find()
        .then(logs => res.json(logs))
        .catch(err => res.status(400).json(`Error: ` + err));
});

router.route('/add').post((req, res) => {
    const code = req.body.code;
    const name = req.body.name;
    const description = req.body.description;

    const newError = new Errorcode({
        code,
        name,
        description
    });

    newError.save()
        .then(() => res.json('Error added!'))
        .catch(err => res.status(400).json(`Error: ` + err));
});

module.exports = router;
const router = require('express').Router();
let runCommand = require('../models/runcommand.model');

router.route('/').get((req, res) => {
    runCommand.find()
        .then(logs => res.json(logs))
        .catch(err => res.status(400).json(`Error: ` + err));
});

router.route('/add').post((req, res) => {
    const commandShort = req.body.commandShort;
    const commandName = req.body.commandName;
    const commandDescription = req.body.commandDescription;
    const response = req.body.response;

    const newRunCommand = new runCommand({
        commandShort,
        commandName,
        commandDescription,
        response
    });

    newRunCommand.save()
        .then(() => res.json('Runcommand created!'))
        .catch(err => res.status(400).json(`Error: ` + err));
});

module.exports = router;
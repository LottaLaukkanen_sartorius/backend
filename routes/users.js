const router = require('express').Router();

const bcrypt = require('bcrypt');
const saltRounds = 10;

const jwt = require('jsonwebtoken');

let User = require('../models/user.model');

router.route('/').get((req, res) => {
    User.find()
        .then(users => res.json(users))
        .catch(err => res.status(400).json(`Error: ` + err));
});

router.route('/register').post((req, res) => {
    const username = req.body.username;
    const cleanPassword = req.body.password;
    const email = req.body.email;
    const notes = req.body.notes;

    bcrypt.hash(cleanPassword, saltRounds, (err, hash) => {
        const password = hash;
        const newUser = new User({
            username,
            password, 
            email,
            notes
        });

        newUser.save()
        .then(() => res.json('User added!'))
        .catch(err => res.status(400).send({...err, error: true}));
    });
});

router.route('/:id').get((req, res) => {
    User.findById(req.params.id)
        .then(user => res.json(user))
        .catch(err => res.status(400).json(`Error: ` + err))
})

router.route('/delete/:id').delete((req, res) => {
    User.findByIdAndDelete(req.params.id)
        .then(() => res.json(true))
        .catch(err => res.status(400).json(`Error: ` + err));
})

router.route('/update/:id').post((req, res) => {
    User.findById(req.params.id)
        .then(user => {
                bcrypt.hash(req.body.password, saltRounds, (err, hash) => {
                user.password = hash;
                user.username = req.body.username;
                user.email = req.body.email;
                user.notes = req.body.notes;
                
                user.update()
                .then(() => res.json('User information updated!' + req.body.password))
                .catch(err => res.status(400).json(`Error: ` + err));
            });
        })
        .catch(err => res.status(400).send({...err, error: true}));
});

router.route('/updatenopswd/:id').post((req, res) => {
    User.findById(req.params.id)
        .then(user => {
                user.password = user.password;
                user.username = req.body.username;
                user.email = req.body.email;
                user.notes = req.body.notes;
                
                user.save()
                .then(() => res.json('User information updated!'))
                .catch(err => res.status(400).json(`Error: ` + err));
        })
        .catch(err => res.status(400).send({err}));
});

router.route('/login').post(async (request, response) => {
    const body = request.body
  
    const user = await User.findOne({ username: body.username });

    const passwordCorrect = user === null ?
      false :
      await bcrypt.compare(body.password, user.password).catch(err => console.log(err));
  
    if ( !(user && passwordCorrect) ) {
      return response.status(401).json({ error: 'invalid username or password' })
    }
  
    const userForToken = {
      username: user.username,
      id: user._id
    }
  
    const token = jwt.sign(userForToken, process.env.SECRET);
  
    response.status(200).send({ 
        token, 
        username: user.username,
        notes: user.notes, 
        email: user.email, 
        userID: user._id
    })
  })

module.exports = router;
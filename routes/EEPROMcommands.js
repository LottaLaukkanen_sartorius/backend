const router = require('express').Router();
let EEPROMcommand = require('../models/eepromcommand.model');

router.route('/').get((req, res) => {
    EEPROMcommand.find()
        .then(logs => res.json(logs))
        .catch(err => res.status(400).json(`Error: ` + err));
});

router.route('/add').post((req, res) => {
    const commandShort = req.body.commandShort;
    const commandName = req.body.commandName;
    const commandDescription = req.body.commandDescription;
    const parameter = req.body.parameter;

    const newEEPROMcommand = new EEPROMcommand({
        commandShort,
        commandName,
        commandDescription,
        parameter
    });

    newEEPROMcommand.save()
        .then(() => res.json('EEPROMcommand created!'))
        .catch(err => res.status(400).json(`Error: ` + err));
});

module.exports = router;
const router = require('express').Router();
let Button = require('../models/button.model');

router.route('/').get((req, res) => {
    Button.find()
        .then(logs => res.json(logs))
        .catch(err => res.status(400).json(`Error: ` + err));
});

router.route('/add').post((req, res) => {
    const code = req.body.code;
    const name = req.body.name;
    const description = req.body.description;
    const response = req.body.description;

    const newButtonCommand = new Button({
        code,
        name,
        description,
        response
    });

    newButtonCommand.save()
        .then(() => res.json('Button added!'))
        .catch(err => res.status(400).json(`Error: ` + err));
});

module.exports = router;
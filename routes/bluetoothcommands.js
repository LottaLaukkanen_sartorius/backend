const router = require('express').Router();
let BTcommand = require('../models/bluetoothcommand.model');

router.route('/').get((req, res) => {
    BTcommand.find()
        .then(logs => res.json(logs))
        .catch(err => res.status(400).json(`Error: ` + err));
});

router.route('/add').post((req, res) => {
    const commandShort = req.body.commandShort;
    const commandName = req.body.commandName;
    const commandDescription = req.body.commandDescription;
    const parameter = req.body.parameter;
    const response = req.body.response;

    const newBTcommand = new BTcommand({
        commandShort,
        commandName,
        commandDescription,
        parameter,
        response
    });

    newBTcommand.save()
        .then(() => res.json('BTcommand created!'))
        .catch(err => res.status(400).json(`Error: ` + err));
});

module.exports = router;
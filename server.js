const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

require('dotenv').config();

const app = express();
const port = process.env.PORT || 5000;

app.use(cors());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept"
    );
    next();
  });

app.use(express.json());

mongoose.set('useFindAndModify', false);

const uri = process.env.ATLAS_URI;
mongoose.connect(uri, { useNewUrlParser:true, useCreateIndex: true });
const connection = mongoose.connection;
connection.once('open', () => {
    console.log("MongoDB database connection established!");
})

const logRouter = require('./routes/logItems');
const usersRouter = require('./routes/users');
const buttonCommandRouter = require('./routes/buttonCommands');
const colorRouter = require('./routes/colorcodes');
const errorRouter = require('./routes/errorcodes');
const btCommandRouter = require('./routes/bluetoothcommands');
const displayCommandRouter = require('./routes/displaycommands');
const EEPROMcommandRouter = require('./routes/EEPROMcommands');
const runCommandRouter = require('./routes/runcommands');
const settingCommandRouter = require('./routes/settingcommands');

app.use('/eventlog', logRouter);
app.use('/users', usersRouter);
app.use('/buttoncommands', buttonCommandRouter);
app.use('/colors', colorRouter);
app.use('/errorcodes', errorRouter);
app.use('/bluetoothcommands', btCommandRouter);
app.use('/displaycommands', displayCommandRouter);
app.use('/eepromcommands', EEPROMcommandRouter);
app.use('/runcommands', runCommandRouter);
app.use('/settingcommands', settingCommandRouter);

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
})
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ButtonSchema = new Schema(
{   
    code: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    response: {
        type: String
    } 
}, 
{
    timestamps: true
}
);

const Buttoncommand = mongoose.model('Buttoncommand', ButtonSchema);

module.exports = Buttoncommand;
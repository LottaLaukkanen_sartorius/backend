const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const errorSchema = new Schema(
{
    code: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String
    }
}, 
{
    timestamps: true
}
);

const Errorcode = mongoose.model('Errorcode', errorSchema);

module.exports = Errorcode;
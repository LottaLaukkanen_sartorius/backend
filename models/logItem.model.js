const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const logItemSchema = new Schema(
{
    timestamp: {
        type: String,
    },
    logID: {
        type: String,
        required: true
    },
    userID: {
        type: String,
        required: true
    },
    device: {
        type: String,
    },
    deviceID: {
        type: String,
    },
    success: {
        type: String
    },
    commandShort: {
        type: String
    },
    commandName: {
        type: String
    },
    sent: {
        type: String
    },
    response: {
        type: String
    },
    fullData: {
        type: Object,
        required: true
    }
}, 
{
    timestamps: true
}
);

const CommandLog = mongoose.model('CommandLog', logItemSchema);

module.exports = CommandLog;
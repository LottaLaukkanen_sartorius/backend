const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const displaycommandSchema = new Schema(
{
    commandShort: {
        type: String,
        required: true,
        maxlength: 2,
        minlength: 2
    },
    commandName: {
        type: String,
        required: true
    },
    commandDescription: {
        type: String
    },
    response: {
        type: Schema.Types.Mixed,
    }
}, 
{
    timestamps: true
}
);

const displayCommand = mongoose.model('displayCommand', displaycommandSchema);

module.exports = displayCommand;
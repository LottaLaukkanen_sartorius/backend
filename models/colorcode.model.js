const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const colorSchema = new Schema(
{
colorcodes: {
    type: Array,
},
}, 
{
    timestamps: true
}
);

const Colors = mongoose.model('Colors', colorSchema);

module.exports = Colors;
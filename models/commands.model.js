const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const commandsSchema = new Schema(
{
_comment: {
    type: Object,
},
displayCommands: {
    type: Array,
},
settingCommands: {
    type: Array,
},
runCommands: {
    type: Array,
},
EEPROMcommands: {
    type: Array,
},
bluetoothCommands: {
    type: Array,
},
}, 
{
    timestamps: true
}
);

const Commands = mongoose.model('Commands', commandsSchema);

module.exports = Commands;
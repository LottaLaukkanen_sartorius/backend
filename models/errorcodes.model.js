const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const errorSchema = new Schema(
{
_comment: {
    type: Object,
},
errorcodes: {
    type: Array,
},
}, 
{
    timestamps: true
}
);

const Errors = mongoose.model('Errors', errorSchema);

module.exports = Errors;
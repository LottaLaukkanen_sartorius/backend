const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ColorSchema = new Schema(
{
    name: {
        type: String,
    },
    hexcode: {
        type: String,
        required: true
    },
    DL: {
        type: String, //when comparing to data polled from pipette, the response data is always string. Despite being actually float
    },
    DH: {
        type: String,
    }
}, 
{
    timestamps: true
}
);

const Colorcode = mongoose.model('Colorcode', ColorSchema);

module.exports = Colorcode;
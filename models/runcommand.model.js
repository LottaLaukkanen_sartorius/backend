const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const runcommandSchema = new Schema(
{
    commandShort: {
        type: String,
        required: true,
        maxlength: 2,
        minlength: 2
    },
    commandName: {
        type: String,
        required: true
    },
    commandDescription: {
        type: String
    },
    parameter: {
        type: Schema.Types.Mixed,
    },
}, 
{
    timestamps: true
}
);

const runCommand = mongoose.model('runCommand', runcommandSchema);

module.exports = runCommand;